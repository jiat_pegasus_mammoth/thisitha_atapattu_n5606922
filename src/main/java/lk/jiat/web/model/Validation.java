package lk.jiat.web.model;

public class Validation {

    private String message;
    private String value;

    public Validation(String message, String value) {

        this.setMessage(message);
        this.setValue(value);

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
