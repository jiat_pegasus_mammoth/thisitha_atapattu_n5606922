package lk.jiat.web.db;

import lk.jiat.web.util.ApplicationProperties;

import java.sql.*;

public class DbConnection implements AutoCloseable{

    private  Connection connection;


    public  Connection getConnection() throws ClassNotFoundException, SQLException {
        if(connection==null){


        ApplicationProperties properties = ApplicationProperties.getInstance();
        Class.forName(properties.get("sql.connection.driver"));
        connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.username"), properties.get("sql.connection.password"));


        }
        return connection;
    }

    public  void iud(String query) {

        try {
            createStatement().executeUpdate(query);

        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();

        }
    }

    public  ResultSet search(String query) throws SQLException, ClassNotFoundException {

        return createStatement().executeQuery(query);


    }

    private  Statement createStatement() throws SQLException, ClassNotFoundException {

        return getConnection().createStatement();

    }

    @Override
    public void close() throws SQLException,ClassNotFoundException {
        getConnection().close();
    }
}
