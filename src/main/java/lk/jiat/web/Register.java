package lk.jiat.web;

import lk.jiat.web.db.DbConnection;
import lk.jiat.web.model.Validation;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.regex.Pattern;

@WebServlet(name = "Register", urlPatterns = "/Register")
public class Register extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String f_name = req.getParameter("f_name");
        String l_name = req.getParameter("l_name");
        String mobile = req.getParameter("mobile");
        String email = req.getParameter("email");

        try (DbConnection dbConnection = new DbConnection()) {

            HttpSession session = req.getSession();

            if (f_name.isEmpty()) {

                session.setAttribute("f_name_validation_reg", new Validation("Please enter the first name.", f_name));

                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("mobile_validation_reg", new Validation(null, mobile));


                session.setAttribute("email_validation_reg", new Validation(null, email));


            } else if (l_name.isEmpty()) {


                session.setAttribute("l_name_validation_reg", new Validation("Please enter the last name.", l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));

                session.setAttribute("mobile_validation_reg", new Validation(null, mobile));

                session.setAttribute("email_validation_reg", new Validation(null, email));


            } else if (mobile.isEmpty()) {


                session.setAttribute("mobile_validation_reg", new Validation("Please enter the contact no.", mobile));


                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));

                session.setAttribute("email_validation_reg", new Validation(null, email));


            } else if (mobile.length() != 10) {


                session.setAttribute("mobile_validation_reg", new Validation("The contact no must have 10 characters.", mobile));


                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));

                session.setAttribute("email_validation_reg", new Validation(null, email));


            } else if (!Pattern.compile("^[0]{1}[7]{1}[01245678]{1}[0-9]{7}$").matcher(mobile).matches()) {


                session.setAttribute("mobile_validation_reg", new Validation("Invalid contact no.", mobile));


                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));

                session.setAttribute("email_validation_reg", new Validation(null, email));


            } else if (dbConnection.search("SELECT * FROM `student` WHERE `mobile`='" + mobile + "';").next()) {


                session.setAttribute("mobile_validation_reg", new Validation("A person with this contact no already exists.", mobile));


                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));

                session.setAttribute("email_validation_reg", new Validation(null, email));


            } else if (email.isEmpty()) {


                session.setAttribute("email_validation_reg", new Validation("Please enter the email address.", email));

                session.setAttribute("mobile_validation_reg", new Validation(null, mobile));

                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));


            } else if (!Pattern.compile("^(.+)@(.+)$").matcher(email).matches()) {


                session.setAttribute("email_validation_reg", new Validation("Invalid email address.", email));

                session.setAttribute("mobile_validation_reg", new Validation(null, mobile));

                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));


            } else if (dbConnection.search("SELECT * FROM `student` WHERE `email`='" + email + "';").next()) {


                session.setAttribute("email_validation_reg", new Validation("A person with this email address already exists.", email));

                session.setAttribute("mobile_validation_reg", new Validation(null, mobile));

                session.setAttribute("l_name_validation_reg", new Validation(null, l_name));

                session.setAttribute("f_name_validation_reg", new Validation(null, f_name));


            } else {


                dbConnection.iud("INSERT INTO `student`(`f_name`,`l_name`,`mobile`,`email`) VALUES('" + f_name + "','" + l_name + "','" + mobile + "','" + email + "');");

                session.setAttribute("success_msg_reg", "Successfully Added!");

            }

            resp.sendRedirect("index.jsp");


        } catch (SQLException | ClassNotFoundException e) {

            e.printStackTrace();


        }


    }
}
