<%@ page import="lk.jiat.web.db.DbConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.jms.ServerSession" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: thisithaatapattu
  Date: 2023-03-29
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User Management</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">

            <div class="row">
                <div class="col-md-4 offset-md-4 mt-5">

                    <div class="row">

                        <h3 class="mt-3 text-success">

                            ${success_msg_reg}

                            <%

                                if (session.getAttribute("success_msg_reg") != null) {
                                    session.removeAttribute("success_msg_reg");
                                }
                            %>

                        </h3>


                        <form action="Register" method="POST" id="register_frm">
                            <div class="mb-3">
                                <label for="f_name" class="form-label">First Name</label>
                                <input type="text" class="form-control" id="f_name"
                                       aria-describedby="emailHelp" name="f_name" value="${f_name_validation_reg.value}">
                                <p class="mt-3 text-danger" >
                                    ${f_name_validation_reg.message}

                                    <%

                                        if (session.getAttribute("f_name_validation_reg") != null) {
                                            session.removeAttribute("f_name_validation_reg");
                                        }



                                    %>

                                </p>
                            </div>
                            <div class="mb-3">
                                <label for="l_name" class="form-label">Last Name</label>
                                <input type="text" class="form-control" id="l_name" name="l_name"
                                       aria-describedby="emailHelp" value="${l_name_validation_reg.value}">
                                <p class="mt-3 text-danger" > ${l_name_validation_reg.message}

                                    <%

                                        if (session.getAttribute("l_name_validation_reg") != null) {
                                            session.removeAttribute("l_name_validation_reg");
                                        }
                                    %>

                                </p>
                            </div>

                            <div class="mb-3">
                                <label for="mobile" class="form-label">Contact No</label>
                                <input type="text" class="form-control" id="mobile" name="mobile"
                                       aria-describedby="emailHelp" value="${mobile_validation_reg.value}">
                                <p class="mt-3 text-danger" > ${mobile_validation_reg.message}

                                    <%

                                        if (session.getAttribute("mobile_validation_reg") != null) {
                                            session.removeAttribute("mobile_validation_reg");
                                        }
                                    %>

                                </p>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email Address</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       aria-describedby="emailHelp" value="${email_validation_reg.value}">

                                <p class="mt-3 text-danger" > ${email_validation_reg.message}

                                    <%

                                        if (session.getAttribute("email_validation_reg") != null) {
                                            session.removeAttribute("email_validation_reg");
                                        }
                                    %>

                                </p>
                            </div>
                            <%--                            <div class="mb-3">--%>
                            <%--                                <label for="exampleInputPassword1" class="form-label">Password</label>--%>
                            <%--                                <input type="password" class="form-control" id="exampleInputPassword1">--%>
                            <%--                            </div>--%>
                            <%--                            <div class="mb-3 form-check">--%>
                            <%--                                <input type="checkbox" class="form-check-input" id="exampleCheck1">--%>
                            <%--                                <label class="form-check-label" for="exampleCheck1">Check me out</label>--%>
                            <%--                            </div>--%>
                            <button type="submit" class="btn btn-primary" >Register</button>
                        </form>
                    </div>


                </div>

            </div>

<%--            <%--%>
<%--                if(true){--%>
<%--                    return;--%>
<%--                }--%>
<%--            %>--%>

            <div class=" px-md-5 mt-5">

                <div class="row">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Email Address</th>
                            <th scope="col">Contact No</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            try (DbConnection dbConnection = new DbConnection()) {
                                ResultSet rs = dbConnection.search("SELECT * FROM `student`;");

                                while (rs.next()) {
                        %>


                        <tr>
                            <td><%=rs.getString("f_name") + " " + rs.getString("l_name")%>
                            </td>
                            <td><%=rs.getString("email")%>
                            </td>
                            <td><%=rs.getString("mobile")%>
                            </td>
                        </tr>

                        <%
                                }
                            } catch (SQLException | ClassNotFoundException e) {

                                e.printStackTrace();


                            }


                        %>

                        </tbody>
                    </table>

                </div>

            </div>


        </div>
    </div>
</div>


</body>

</html>